# Returns the path to the dataset file.
getDatasetFilePath <- function(datasetName) {
    paste0("../../data/public/", datasetName, ".csv")
}

# Loads Iris dataset, sets its labels and displays its summary.
#' @export
loadIris <- function(show = FALSE) {
    data(iris)
    iris$Species <- as.factor(iris$Species)
    attr(iris, "datasetName") <- "iris"
    attr(iris, "variables") <- getIrisVariables()
    attr(iris, "output") <- "Species"
    if (show) glance(iris)
    iris
}

# Retuns variables of Iris dataset.
getIrisVariables <- function() {
   list(
        numericVariable("Sepal.Length", "longueur de sepale"),
        numericVariable("Sepal.Width", "largeur de sepale"),
        numericVariable("Petal.Length", "longueur de petale"),
        numericVariable("Petal.Width", "largeur de petale"),
        factorVariable("Species", "espece")
    ) %>% variablesFromList
}

# Loads Covertype dataset, sets its labels and displays its summary.
#' @export
loadCover <- function(show = FALSE, nbrRows = NULL, filePath = NULL) {
    if (is.null(filePath)) {
        filePath <- getDatasetFilePath("covertype")
    }
    cover <- read.csv(
        file = filePath,
        header = FALSE,
        sep = ","
    )
    if (!is.null(nbrRows)) {
        cover <- cover[sample(nrow(cover), nbrRows), ]
    }
    names(cover)[55] <- "type"
    attr(cover, "datasetName") <- "covertype"
    attr(cover, "variables") <- getCoverVariables()
    attr(cover, "output") <- "type"
    cover$type <- as.factor(cover$type)
    setClassVariables(cover)
    if (show) glance(cover)
    cover
}

setClassVariables <- function(df) {
    for (varName in getNumericVariables(df)) {
        df[[varName]] <- as.numeric(df[[varName]])    
    }
    for (varName in getFactorVariables(df)) {
        df[[varName]] <- as.factor(df[[varName]])
    }
}

getCoverVariables <- function() {
    variables <- list(
        numericVariable("V1", "altitude"),
        numericVariable("V2", "aspect"),
        numericVariable("V3", "pente"),
        numericVariable("V4", "distance eau horizontale"),
        numericVariable("V5", "distance eau verticale"),
        numericVariable("V6", "distance route"),
        numericVariable("V7", "ombre matin"),
        numericVariable("V8", "ombre midi"),
        numericVariable("V9", "ombre apres-midi"),
        numericVariable("V10", "distance feu horizontale")
    )
    for (i in 1:4) {
        index <- i + 10
        variables[[index]] <- factorVariable(
            paste0("V", index), paste0("region ", i))
    }
    for (i in 1:40) {
        index <- i + 14
        variables[[index]] <- factorVariable(
            paste0("V", index), paste0("sol ", i))
    }
    variables[[55]] <- factorVariable("type", "couverture")
    variablesFromList(variables)
}

# Loads BCWD dataset, sets its labels and displays its summary.
#' @export
loadBCWD <- function(show = FALSE) {
    bcwd <- read.csv(
        file = getDatasetFilePath("bcwd"),
        header = FALSE,
        sep = ","
    )
    bcwd <- bcwd[-1]
    colnames(bcwd) <- c(
        "diagnosis",
        "radius_mean",
        "texture_mean",
        "perimeter_mean",
        "area_mean",
        "smoothness_mean",
        "compactness_mean",
        "concavity_mean",
        "concave points_mean",
        "symmetry_mean",
        "fractal_dimension_mean",
        "radius_se",
        "texture_se",
        "perimeter_se",
        "area_se",
        "smoothness_se",
        "compactness_se",
        "concavity_se",
        "concave points_se",
        "symmetry_se",
        "fractal_dimension_se",
        "radius_worst",
        "texture_worst",
        "perimeter_worst",
        "area_worst",
        "smoothness_worst",
        "compactness_worst",
        "concavity_worst",
        "concave points_worst",
        "symmetry_worst",
        "fractal_dimension_worst"
    )
    bcwd$diagnosis <- as.factor(bcwd$diagnosis)
    attr(bcwd, "datasetName") <- "bcwd"
    attr(bcwd, "variables") <- getBCWDVariables()
    attr(bcwd, "output") <- "diagnosis"
    setClassVariables(cover)
    if (show) glance(bcwd)
    bcwd
}

# Retuns variables of Iris dataset.
getBCWDVariables <- function() {
    list(
        numericVariable("radius_mean", "rayon moyen"),
        numericVariable("texture_mean", "texture moyenne"),
        numericVariable("perimeter_mean", "perimetre moyen"),
        numericVariable("area_mean", "surface moyenne"),
        numericVariable("smoothness_mean", "granularite moyenne"),
        numericVariable("compactness_mean", "compacite moyenne"),
        numericVariable("concavity_mean", "concavite moyenne"),
        numericVariable("concave points_mean", "nombre de concavies"),
        numericVariable("symmetry_mean", "symetrie moyenne"),
        numericVariable("fractal_dimension_mean", "dimension fractale moyenne"),
        numericVariable("radius_se", "rayon ecart-type"),
        numericVariable("texture_se", "texture ecart-type"),
        numericVariable("perimeter_se", "perimetre ecart-type"),
        numericVariable("area_se", "surface ecart-type"),
        numericVariable("smoothness_se", "granularite ecart-type"),
        numericVariable("compactness_se", "compacite ecart-type"),
        numericVariable("concavity_se", "concavite ecart-type"),
        numericVariable("concave points_se", "nombre de concavites"),
        numericVariable("symmetry_se", "symetrie ecart-type"),
        numericVariable("fractal_dimension_se", "dimension fractale ecart-type"),
        numericVariable("radius_worst", "rayon pire"),
        numericVariable("texture_worst", "texture pire"),
        numericVariable("perimeter_worst", "perimetre pire"),
        numericVariable("area_worst", "surface pire"),
        numericVariable("smoothness_worst", "granularite pire"),
        numericVariable("compactness_worst", "compacite pire"),
        numericVariable("concavity_worst", "concavite pire"),
        numericVariable("concave points_worst", "nombre de concavites"),
        numericVariable("symmetry_worst", "symetrie pire"),
        numericVariable("fractal_dimension_worst", "dimension fractale pire"),
        factorVariable("diagnosis", "diagnostic")
    ) %>% variablesFromList
}

