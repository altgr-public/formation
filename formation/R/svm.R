## import::here(e1071, svm)

## import::here(evaluate.R, risk)


fitSvm <- function(splitted) {
    variablesNames <- colnames(splitted$XTesting)
    df <- splitted$XTraining
    df$Y <- splitted$YTraining
    model <- svm(
        Y ~ .,
        data=df,
        type="eps-regression",
        kernel="polynomial",
        degree=2
    )
    summary(model)
    model
}

evaluateSvm <- function(splitted) {
    print("Régression SVM")
    model <- fitSvm(splitted)
    predicted <- predict(model, newdata = splitted$XTesting)
    riskValue <- risk(predicted, splitted$YTesting)
    print("Risque empirique : ")
    print(riskValue)
}
