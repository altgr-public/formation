# Formation société en apprentissage automatique

Attention : ce README est obsolète !

## Généralités

Formation en apprentissage automatique dispensée par la société AltGR.

Ce projet contient :
- la librairie R formation regroupant des fonctions de traitement et d'analyse de
données ;
- des travaux pratiques en apprentissage automatique.

## Installation

### Système

Sous Linux, le paquet cmake est un pré-requis :
```bash
sudo apt install cmake
```

### Librairie formation

Se déplacer à la racine du projet, 
rendre le script bin/install.sh exécutable, puis construire la librairie :
```bash
chmod +x bin/install.sh
bin/build.sh
```
Installer le paquet devtools pour faciliter le développement.
```
install.packages("devtools")
```
À chaque modification de la librairie, mettre à jour la documentation :
```
devtools::document(path = path/to/formation)
```
Puis installer le paquet formation à l'aide de devtools :
```
devtools::install_local(path = path/to/formation)
```
ou (en simulant l'installation) :
```
devtools::load_all(path = path/to/formation)
```

Remarque : les paquets R n'autorisent pas la création de sous-répertoires ;
c'est une contrainte forte qui nuit à l'organisation d'un paquet.

## Organisation

Ce projet est organisé comme suit :
- README.md ;
- LICENSE (licence Gnu GPLv3) ;
- /bin : exécutables ;
- /data : jeux de données :
  * /data/backup : sauvegardes de quelques jeux de données,
  * /data/noise : données avec bruit,
  * /data/public : jeux de données publics ;
- /doc : documentation :
  * /doc/R : documentation de paquets R,
- /graphics : graphiques engendrés par le code R :
- /formation : code source de la librairie formation :
  * /formation/DESCRIPTION : description de la librairie, 
  * /formation/NAMESPACE : fonctions exportées,
  * /formation/man : documentation engendrée par roxygen2,
  * /formation/R : code source ;
- /tp : travaux pratiques :
  * /tp/foret : forêt aléatoire,
  * /tp/xgboost : XGBoost.

De plus, chaque répertoire /tp/{name} est divisée en :
    + /tp/{name}/bin : contient l'exécutables run.R,
    + /tp/{name}/graphics : graphiques engendrés par le code R,
    + /tp/{name}/img : illustrations pour le fichier /tp/{name}/tex/{name}.tex,
    + /tp/{name}/R : code R,
    + /tp/{name}/tex : code LaTeX.

## Thèmes

### Apprentissage de régressions linéaires (non pénalisée, Lasso et ridge) et SVM

### Forêts aléatoires

## Travaux pratiques

### Travaux pratiques

Installer les jeux de données nécessaires. 

#### Covertype dataset

Télécharger l'archive covtype.data.gz depuis le site [UCI Machine Learning
Repository](https://archive.ics.uci.edu/ml/machine-learning-databases/covtype/),
la décompresser dans /data et renommer le dataset covtype.data en cover.csv :
```bash
mv covtype.data covertype.csv
```

### Exécution

Pour exécuter le script d'un séance de travaux pratiques, se placer à la racine
de son répertoire :
```
cd tp/{name}
```
Puis exécuter le script principal :
```
Rscript bin/run.R
```
