---
title: "XGBoost"
output: html_notebook
bibliography: tex/biblio.bib  
---
# Pré-requis

## R et RStudio

Testé sur Windows avec :

* R version 4.2.1 ;
* RStudio version 2022.2.3.492.

Pour vérifier vos versions :

```{r}
R.version
rstudioapi::versionInfo()
```
## Dépendances Linux

Sous Linux, les paquets $\texttt{librsvg2-dev}$ et $\texttt{libnode-dev}$ sont
indispensables pour la visualisation.

## Paquets R

Installer les paquets suivants :

```{r include = FALSE}
install.packages(c(
  "caret",
  "corrplot",
  "Ckmeans.1d.dp",
  "DiagrammeR",
  "DiagrammeRsvg",
  "ggplot2",
  "glmnet",
  "rsvg",
  "factoextra",
  "GGally",
  "scales"
  ))
```

## Librairie $\texttt{xgboost}$

Installer le paquet $\texttt{xgboost}$ et ses fonctions utiles :

```{r include = FALSE}
install.packages("xgboost")
import::here(
    xgboost,
    xgb.cv,
    xgb.importance,
    xgb.plot.importance,
    xgb.plot.tree,
    xgb.set.config,
    xgb.train,
    xgboost
)
```

# Analyse du jeu de données Iris

L'algorithme XGBoost possède de nombreux paramètres. Nous renvoyons à la
documentation
générale~\url{https://xgboost.readthedocs.io/en/latest/parameter.html}, à celle
de l'interface
R~\url{https://cran.r-project.org/web/packages/xgboost/xgboost.pdf} ou au
guide~\url{https://www.kaggle.com/code/prashant111/a-guide-on-xgboost-hyperparameters-tuning/notebook}
pour une description détaillée. 

Voici le plan.

* Étude de l'influence de quelques-uns des paramètres de XGBoost.
* Recherche des meilleurs paramètres.
* Interprétation du modèle obtenu.

## Étude des paramètres

### Préparation

On reprend le jeu de données Iris, déjà analysé dans le TP sur la forêt aléatoire.

```{r }
# Returns a dataframe of variables from a list. Ugly, but alternatives with
# rbind do not return a true dataframe.
variablesFromList <- function(variables) {
    df <- data.frame(variables[[1]])
    for (variable in variables[2 : length(variables)]) {
        df <- rbind(df, variable)
    }
    df
}

# Returns a numeric variable.
numericVariable <- function(name, label) {
    list(name = name, label = label, class = "numeric")
}

# Returns a factor variable.
factorVariable <- function(name, label) {
    list(name = name, label = label, class = "factor")
}

# Returns variables of Iris dataset.
getIrisVariables <- function() {
   list(
        numericVariable("Sepal.Length", "longueur de sepale"),
        numericVariable("Sepal.Width", "largeur de sepale"),
        numericVariable("Petal.Length", "longueur de petale"),
        numericVariable("Petal.Width", "largeur de petale"),
        factorVariable("Species", "espece")
    ) |> variablesFromList()
}

# Loads Iris dataset and set attributes.
loadIris <- function(show = FALSE) {
    data(iris)
    iris$Species <- as.factor(iris$Species)
    attr(iris, "datasetName") <- "iris"
    attr(iris, "variables") <- getIrisVariables()
    attr(iris, "output") <- "Species"
    if (show) glance(iris)
    iris
}

iris <- loadIris()
```

On sépare données d'entraînement et données de validation. De plus, XGBoost a besoin que les classes soient des entiers commençant à 0. On convertit la colonne des espèces d'iris.

```{r}
# Returns a default value if given value is null.
getDefault <- function(x, default) {
    if (is.null(x)) default else x
}

# Splits the dataset between training and testing.
splitData <- function(df) {
    set.seed(42)
    nbrRows <- nrow(df)
    trainingIndices <- sample(1:nbrRows, 0.7 * nbrRows)
    output <- getDefault(attr(df, "output"), "Y")
    list(
        XTraining = df[trainingIndices, colnames(df) != output],
        YTraining = df[trainingIndices, output],
        XTesting = df[-trainingIndices, colnames(df) != output],
        YTesting = df[-trainingIndices, output],
        datasetName = getDefault(attr(df, "datasetName"), "anonymous"),
        output = output
    )
}

# XGBoost only accepts matrices.
# xgb.DMatrix object is not convenient: we cannot print their coefficients.
# Classes must be integers.
toMatrix <- function(splitted) {
    splitted$XTraining <- as.matrix(splitted$XTraining)
    splitted$YTrainingInt <- as.integer(splitted$YTraining) - 1
    splitted$XTesting <- as.matrix(splitted$XTesting)
    splitted    
    ## xgb.DMatrix(data = as.matrix(xs), label = as.integer(y) - 1)
}

splitted <- splitData(iris) |> toMatrix()
```

### Modèle par défaut

Commençons par demander à xgboost d'être discret :

```{r include = FALSE}
xgb.set.config(verbosity = 0)
```

Puis définissons une enveloppe pour notre tâche avec les paramètres suivants :

* $\texttt{booster} = \texttt{gbtree}$ : CART comme prédicteur faible ;
* $\texttt{num_class} = 3$ : le nombre d'espèces d'Iris ;
* $\texttt{objective} = \texttt{multi:softmax}$ : classification multiclasse avec la fonction softmax (les classes doivent être des entiers de 0 à \texttt{num_class} - 1).

```{r}
fitXgboostSoftmax <- function(splitted, nrounds = 100, params = list()) {
    set.seed(42)
    xgboost(
        data = splitted$XTraining,
        label = splitted$YTrainingInt,
        params = params,
        nrounds = nrounds,
        early_stopping_rounds = 10,
        method = "gbtree",
        num_class = nlevels(splitted$YTraining),
        objective = "multi:softmax",
        verbose = FALSE
        )
}
```

### Ordre de l'ajustement des paramètres

Voici l'ordre dans lequel nous allons ajuster les paramètres :

1. Paramètres de convergence acceptables : taux d'apprentissage $\texttt{eta}$ et nombre de prédicteurs $\texttt{nrounds}$.
2. Paramètres de la structure des arbres : profondeur maximale de chaque arbre $\texttt{max\_depth}$ et poids minimum nécessaire pour scission $\texttt{min\_child\_weight}$.
3. Paramètres de sous-échantillonnage : nombre de variables sélectionnées $\texttt{colsample\_bytree}$ et proportion de sous-échantillonnage $\texttt{subsample}$.
4. Paramètre de pénalisation du surapprentissage : seuil de scission $\texttt{gamma}$.
5. Amélioration des paramètres de convergence : taux d'apprentissage $\texttt{eta}$ et nombre de prédicteurs $\texttt{nrounds}$.

Pour tester l'influence de chaque paramètre isolément, nous utiliserons les fonctions suivantes :

```{r}
# Returns the confusion matrix of predictions vs observed.
predictClass <- function(model, splitted) {
    predictedIndices <- predict(model, splitted$XTesting) + 1
    predictedClasses <- levels(splitted$YTesting)[predictedIndices] |> 
        as.factor()
    caret::confusionMatrix(splitted$YTesting, predictedClasses, mode = "everything")
}

# Plots errors for a range of values of a single parameter.
plotSingleParameter <- function(
    name, 
    label, 
    title, 
    values,
    params = list(),
    nrounds = 100,
    ymaxEvolution = 0.2,
    ymaxError = 0.05
    ) {
    nbrValues <- length(values)
    errors <- rep(0, nbrValues)
    colors <- rainbow(nbrValues)
    plot(
        1,
        type = "n",
        xlim = c(0, nrounds),
        ylim = c(0, ymaxEvolution),
        main = paste0("Evolution de l'erreur out-of-bag selon ", title),
        xlab = "Nombre d'iterations",
        ylab = "Erreur",
        cex.main = 0.9    
    )
    for (i in 1:nbrValues) {
        params[name] = values[i]
        model <- fitXgboostSoftmax(splitted, nrounds = nrounds, params = params)
        ys <- model$evaluation_log$train_mlogloss
        xs <- 1:min(nrounds, length(ys))
        lines(xs, ys, col = colors[i])
        result <- predictClass(model, splitted)
        errors[i] <- 1 - result$overall[1]
    }
    legend(
        "topright",
        title = label,
        legend = values,
        col = colors,
        pch = 15,
        bg = "white"
        )
    barplot(
        height = errors,
        main = paste0("Erreur selon ", title),
        xlab = label,
        ylab = "Erreur",
        ylim = c(0, ymaxError),
        names = values,
        col = colors,
        las = 3
        )
}
```

Remarque : on pourrait directement utiliser la validation croisée à cette étape.

### Taux d'apprentissage $\texttt{eta}$

Le paramètre $\texttt{eta}$ est le taux d'apprentissage. Il contrôle la convergence vers un minimum : c'est un facteur multiplicatif appliqué à tous les poids des prédicteurs faibles. La valeur par défaut est $0.3$.

```{r}
plotSingleParameter(
    "eta", 
    "Facteur d'apprentissage", 
    "le facteur \nd'apprentissage (eta).", 
    c(0.01, 0.05, 0.1, 0.3, 0.5, 0.99),
    nrounds = 100,
    ymaxEvolution = 0.9,
    ymaxError = 0.1
)
```
Un taux d'apprentissage à $0.1$ paraît suffisamment rapide pour $100$ itérations.

### Nombre de prédicteurs $\texttt{nrounds}$

Le paramètre \texttt{nrounds} est le nombre d’itérations de boosting, donc le nombre de prédicteurs faibles. Il correspond au nombre \texttt{ntree} d'arbres dans \texttt{randomForest}. 

À l'instar des forêts aléatoires, il est inutile de prendre une valeur trop grande, sauf si le taux d'apprentissage $\texttt{eta}$ est très faible, auquel cas un nombre d'itérations élevé est nécessaire.

Étudions son influence sur l'erreur mesurée par l'entropie croisée * out-of-bag* lorsque $\texttt{eta} = 0.1$:

```{r}
model <- fitXgboostSoftmax(splitted, nrounds = 500, params = list(eta = 0.1))
print(model)
predictClass(model, splitted)
plot(
    model$evaluation_log,
    main = "Evolution de l'erreur out-of-bag en fonction du nombre d'arbres",
    xlab = "Nombre d'arbres",
    ylab = "Erreur",
    type = "l",
    col = "blue"
    )
```

La valeur par défaut $\texttt{nrounds} = 100$ pour notre enveloppe paraît suffisante. C'est celle par défaut pour l'implémentation d'XGBoost en Python.

Remarquons que le paramètre \texttt{early_stopping_rounds} est fixé à $10$ dans notre enveloppe : si l'erreur ne diminue pas durant \texttt{early_stopping_rounds} itérations, XGBoost renverra la dernière itération où cette erreur a diminué (la première fois qu'une telle stagnation se produit).

```{r}
model <- fitXgboostSoftmax(splitted, params = list(gamma = 0.5))
print(model$best_msg)
```

Dans ce cas, on peut s'arrêter à la $62$-ième itération.

###  Profondeur maximale de chaque arbre $\texttt{max\_depth}$

Le paramètre $\texttt{max\_depth}$ est relié aux paramètres $\texttt{maxnodes}$ et $\texttt{nodesize}$ de $\texttt{randomForest}$ : il contrôle la profondeur maximale des arbres pour prévenir le surapprentissage. La valeur par défaut est $6$.
```{r}
plotSingleParameter(
    "max_depth", 
    "Profondeur maximale", 
    "la profondeur \nmaximale (max_depth).", 
    c(1, 2, 3, 6, 15, 50)
)
```

Les résultats semblent similaires pour le jeu de données Iris. Gardons la valeur par défaut de $6$.

### Poids minimum nécessaire pour scission $\texttt{min\_child\_weight}$ 

Le paramètre $\texttt{min\_child\_weight}$ est le minimum que doit atteindre la somme d'un poids associé aux résidus pour scinder un noeud : si ce seuil minimum n'est pas atteint, alors le noeud n'est pas scindé en deux dans l'arbre. La valeur par défaut est $1$.

```{r}
plotSingleParameter(
    "min_child_weight", 
    "Somme minimale des poids", 
    "\nla somme minimale des poids (min_child weight).", 
    c(0, 0.1, 1, 10),
    ymaxEvolution = 0.9,
    ymaxError = 0.1
)
```

La valeur $0$ paraît convenable pour ce jeu de données ; vérifier le surapprentissage.

### Nombre de variables sélectionnées $\texttt{colsample\_bytree}$

Le paramètre \texttt{colsample\_bytree} est la proportion de variables sélectionnées pour
  construire un arbre. C'est l'équivalent du nombre de variables \texttt{mtry} de \texttt{randomForest}. La valeur par défaut est $1$. Testons différentes valeurs :
```{r}
plotSingleParameter(
    "colsample_bytree", 
    "proportion de variables", 
    "la proportion de \nvariables selectionnees (colsample_bytree).", 
    c(0.25, 0.5, 0.75, 1)
)
```
  
À l'instar de l'analyse du jeu de données Iris via les forêts aléatoires, $2$ variables semblent convenir.

### Proportion de sous-échantillonnage $\texttt{subsample}$ 

Le paramètre $\texttt{subsample}$ est la proportion du sous-échantillon sur lequel chaque arbre est entraîné. Diminue le surapprentissage. La valeur par défaut est $1$ : chaque prédicteur faible apprend l'intégralité des données d'entraînement.

```{r}
plotSingleParameter(
    "subsample", 
    "Proportion de sous-échantillonnage", 
    "\nla proportion du sous-échantillonnage (subsample).", 
    c(0.1, 0.3, 0.5, 0.7, 0.9, 1),
    ymaxEvolution = 0.9,
    ymaxError = 0.1
)
```
Le jeu de données Iris étant de cardinal faible, on peut ne pas sous-échantillonner.

### Seuil de scission $\texttt{gamma}$

Le paramètre $\texttt{gamma}$ est la diminution minimale du coût pour scinder un noeud. L'augmenter diminue donc le nombre de feuilles, donc le surapprentissage. La valeur par défaut est $0$ (pas de pénalisation).

```{r}
plotSingleParameter(
    "gamma", 
    "Diminution minimale du coût", 
    "\nla diminution minimale du coût (gamma).", 
    c(0, 0.1, 1, 10),
    ymaxEvolution = 0.9,
    ymaxError = 0.1
)
```

Lorsque la pénalisation est importante, le modèle stagne et l'algorithme s'arrête prématurément. Pénalisons avec une valeur de $0.1$ pour éviter le surapprentissage tout en gardant de bonnes performances.

### Retour sur $\texttt{eta}$ et $\texttt{nrounds}

Après l'étude sommaire précédente, les valeurs suivantes des paramètres semblent convenir pour l'erreur (optimiste) * out-of-bag * :

* $\texttt{max\_depth} = 6$ ;
* $\texttt{min\_child\_weight} = 0$ ;
* $\texttt{colsample\_bytree} = 0.5$ ;
* $\texttt{subsample} = 1$.

Remarquons que les performances sur les données de validation sont sensiblement les mêmes…

On cherche maintenant à optimiser le taux d'apprentissage $\texttt{eta}$ et le nombre d'itérations $\texttt{nrounds}$ pour ces valeurs.

```{r}
params <- list(
    max_depth = 6,
    min_child_weight = 0,
    colsample_bytree = 0.5,
    subsample = 1
)
plotSingleParameter(
    "eta", 
    "Facteur d'apprentissage", 
    "le facteur \nd'apprentissage (eta).", 
    c(0.05, 0.1, 0.15),
    nrounds = 200,
    params = params,
    ymaxEvolution = 0.9,
    ymaxError = 0.1
)
```

Gardons $\texttt{eta} = 0.1$ et $\texttt{nrounds} = 100$.

## Optimisation par grille

Nous allons maintenant optimiser les paramètres simultanément par validation croisée sur une grille de valeurs, via la fonction $\texttt{xgb.cv}$.

```{r}
findBestParamsXgboost <- function(splitted, paramsGrid, nrounds = 500) {
    result = list(error = Inf, index = 0, params = list())
    for (params in paramsGrid) {
        params <- as.list(params)
        set.seed(42)
        modelCV <- xgb.cv(
            data = splitted$XTraining,
            label = splitted$YTrainingInt,
            params = params,
            nfold = 5,
            nrounds = nrounds,
            early_stopping_rounds = 10,
            method = "gbtree",
            num_class = nlevels(splitted$YTraining),
            objective = "multi:softmax",
            verbose = FALSE,           
            prediction = TRUE
        )
        index <- modelCV$best_iteration
        error <- modelCV$evaluation_log[index, test_mlogloss_mean]
        if (error < result$error) {
            print(error)
            result$error <- error
            result$params <- params
            result$index <- index
        }
    }
    result
}
```

Nous créons la grille (restreinte) de paramètres suivante :

```{r}
paramsGrid <- expand.grid(
        colsample_bytree = c(0.5, 0.75, 1),
        max_depth = c(3, 6),
        eta = c(0.1, 0.3, 0.99),
        gamma = c(0, 0.1, 1),
        subsample = c(0.5, 1),
        min_child_weight = c(0, 0.1)
    ) |> (\(grid) split(grid, seq(nrow(grid))))()
```

Résultat :

```{r}
bestParams <- findBestParamsXgboost(splitted, paramsGrid, nrounds = 100)
print("Erreur : ")
print(bestParams$error)
print("Itération : ")
print(bestParams$index)
paramNames <- c(
    "colsample_bytree",
    "max_depth",
    "eta",
    "gamma",
    "subsample",
    "min_child_weight"
)
for (paramName in paramNames) {
    print(paramName)
    print(bestParams$params[paramName])
}
```

Les paramètres $\texttt{eta}$, $\texttt{max\_depth}$ et $\texttt{subsample}$ diffèrent de ceux définis dans la sous-section précédente. D'un autre côté, la routine précédente favorise les valeurs des paramètres les plus basses.

Observons que l'erreur est minimale au bout de la sixième itération.

On entraîne ensuite XGBoost avec les meilleurs paramètres trouvés :

```{r}
model <- fitXgboostSoftmax(splitted, params = bestParams$params)
print(model)
predictClass(model, splitted)
plot(
    model$evaluation_log,
    main = "Evolution de l'erreur out-of-bag en fonction du nombre d'arbres",
    xlab = "Nombre d'arbres",
    ylab = "Erreur",
    type = "l",
    col = "blue"
    )
```
## Interprétation

Classons les variables explicatives par ordre d'importance avec la fonction $\texttt{xgb.importance}$.

```{r}
# Returns the names of the explanatory variables.
explanatoryVariablesNames <- function(df) {
    colnames(df)[colnames(df) != attr(df, "output")]
}

xgb.importance(
  feature_names = explanatoryVariablesNames(iris), model = model) |>
    xgb.plot.importance()
```

Traçons le premier arbre de décision avec $\texttt{xgb.plot.tree}$ :
```{r}
xgb.plot.tree(
    feature_names = explanatoryVariablesNames(iris),
    model = model,
    trees = 1
    )
```

Prédiction de l'espèce sur les données de validation :
```{r}
predictClass(model, splitted)
```

# Analyse du jeu de données Covertype

## Présentation du jeu de données

Le jeu de données Covertype décrit le type de couverture forestière pour 581
012 observations de 54 variables continues ou discrètes, notamment les suivantes :

* altitude ;
* aspect ;
* pente ;
* distance à une source d'eau ;
* ensoleillement ;
* type de sol.

Voir la [page web](https://archive.ics.uci.edu/ml/datasets/ĉovertype) pour plus
de détails, ainsi que le fichier associé $\texttt{covtype.info}$ à l'archive
téléchargeable [ici](https://archive.ics.uci.edu/ml/machine-learning-databases/covtype/).

<div align="center">
  <img src="img/map.png"/>
  <p>Plan de la forêt nationale de Roosevelt.</p>
</div>

Localisations :
<center>
  <div><img src="img/rawah.jpg" width="200" height="200"/><p>Rawah</p></div>
  <div><img src="img/cache_la_poudre.jpg" width="200" height="200"/><p>Cache La Poudre</p></div>
  <div align="center"><img src="img/comanche.jpg" width="200" height="200"/><p>Comanche Peak</p><div>
  <div align="center"><img src="img/neota.jpg" width="200" height="200"/><p>Neota</p><div>
</center>

Types d'arbres :
<center>
  <div><img src="img/spruce.jpg" width="200" height="200"/><p>Sapinière</p></div>
  <div><img src="img/lodgepole.jpg" width="200" height="200"/><p>Pin tordu</p></div>
  <div><img src="img/ponderosa.jpg" width="200" height="200"/><p>Pin jaune</p></div>
  <div><img src="img/cottonwood.jpg" width="200" height="200"/><p>Peuplier/Saule</p></div>
  <div><img src="img/aspen.jpg" width="200" height="200"/><p>Tremble</p></div>
  <div><img src="img/douglas.jpg" width="200" height="200"/><p>Douglas</p></div>
  <div><img src="img/krummholz.jpg" width="200" height="200"/><p>Krummholz</p></div>
</center>

L'article récent @kumar20 entraîne une forêt aléatoire sur ce jeu de
données (sans donner ses paramètres) et compare les performances d'autres
algorithmes. Cette [page
web](https://www.kaggle.com/code/osterburg/forest-cover-type/notebook) analyse ce jeu de données avec Python, pour une compétition Kaggle.

## Préparation des données 

1. Télécharger le [fichier](https://archive.ics.uci.edu/ml/machine-learning-databases/covtype/covtype.data.gz).
2. Le décompresser.
3. Le renommer $\texttt{covertype.csv}$.
4. Le déplacer dans le répertoire $\texttt{/data/public/}$.

Vérifier que le jeu de données se charge bien. Changer le chemin d'acces !

```{r}
# Returns a dataframe of variables from a list. Ugly, but alternatives with
# rbind do not return a true dataframe.
variablesFromList <- function(variables) {
    df <- data.frame(variables[[1]])
    for (variable in variables[2 : length(variables)]) {
        df <- rbind(df, variable)
    }
    df
}

# Returns covertype dataset variables.
getCoverVariables <- function() {
    variables <- list(
        numericVariable("altitude", "altitude"),
        numericVariable("aspect", "azimuth pente"),
        numericVariable("slope", "pente"),
        numericVariable("distanceWaterH", "distance eau horizontale"),
        numericVariable("distanceWaterV", "distance eau verticale"),
        numericVariable("distanceRoad", "distance route"),
        numericVariable("shadowMorning", "ombre matin"),
        numericVariable("shadowNoon", "ombre midi"),
        numericVariable("shadowAfternoon", "ombre apres-midi"),
        numericVariable("distanceFire", "distance feu")
    )
    for (i in 1:4) {
        index <- i + 10
        variables[[index]] <- factorVariable(
            paste0("V", index), paste0("region ", i))
    }
    for (i in 1:40) {
        index <- i + 14
        variables[[index]] <- factorVariable(
            paste0("V", index), paste0("sol ", i))
    }
    variables$type <- factorVariable("type", "couverture")
    variables$shadow <- numericVariable("shadow", "ombre")
    variables$distanceWater <- numericVariable("distanceWater", "distance eau")
    variables$region <- factorVariable("region", "region")
    variables$soil <- factorVariable("soil", "sol")
    variables$hasWater 
    variablesFromList(variables)
}

# Returns the names of all numeric variables of a dataset.
getNumericVariables <- function(df) {
    variables <- attr(df, "variables")
    variables$name[variables$class == "numeric"]
}

# Returns the names of all factors of a dataset.
getFactorVariables <- function(df) {
    variables <- attr(df, "variables")
    output <- attr(df, "output")
    variables$name[(variables$class == "factor") & (variables$name != output)]
}

# Sets class variables.
setClassVariables <- function(df) {
    for (varName in getNumericVariables(df)) {
        df[[varName]] <- as.numeric(df[[varName]])    
    }
    for (varName in getFactorVariables(df)) {
        df[[varName]] <- as.factor(df[[varName]])
    }
}

# Renames some variables.
renameVariables <- function(cover) {
    names(cover)[1] <- "altitude"
    names(cover)[2] <- "aspect"
    names(cover)[3] <- "slope"
    names(cover)[4] <- "distanceWaterH"
    names(cover)[5] <- "distanceWaterV"
    names(cover)[6] <- "distanceRoad"
    names(cover)[7] <- "shadowMorning"
    names(cover)[8] <- "shadowNoon"
    names(cover)[9] <- "shadowAfternoon"
    names(cover)[10] <- "distanceFire"
    names(cover)[55] <- "type"
    cover
}

# Loads Covertype dataset, sets its labels and displays its summary.
loadCover <- function(filePath, show = FALSE, nbrRows = NULL) {
    cover <- read.csv(
        file = filePath,
        header = FALSE,
        sep = ","
    )
    if (!is.null(nbrRows)) {
        cover <- cover[sample(nrow(cover), nbrRows), ]
    }
    cover <- renameVariables(cover)
    cover$distanceWater <- sqrt(cover$distanceWaterH ** 2 + cover$distanceWaterV ** 2)
    cover$shadow <- cover$shadowMorning + cover$shadowNoon + cover$shadowAfternoon
    cover$region <- max.col(cover[, paste0("V", 11:14)])
    cover$soil <- max.col(cover[, paste0("V", 15:54)])
    attr(cover, "datasetName") <- "covertype"
    attr(cover, "variables") <- getCoverVariables()
    attr(cover, "output") <- "type"
    cover$type <- as.factor(cover$type)
    setClassVariables(cover)
    if (show) glance(cover)
    cover
}

filePath <- "/home/antoine/altgr/code/formation/data/public/covertype.csv"
cover <- loadCover(filePath = filePath)
```

Pas de valeurs manquantes a priori :

```{r}
is.na(cover) |> sum() |> print()
```

## Analyse exploratoire

Attention : les classes sont déséquilibrées.

```{r}
# Returns the strings with capitalized first letters.
capitalize <- function(xs) {
    paste0(toupper(substr(xs, 1, 1)), substr(xs, 2, nchar(xs)))
}

# Returns the label of a variable.
labelFromName <- function(df, varName) {
    variables <- attr(df, "variables")
    variables$label[variables$name == varName][1]
}

# Returns the output label.
outputLabel <- function(df) {
    attr(df, "output") |> (\(output) labelFromName(df, output))()
}

# Returns the output variable.
outputColumn <- function(df) df[[attr(df, "output")]]

# Returns a palette that depends on the number of rows of a given dataframe.
paletteColors <- function(df, hasAlpha = FALSE) {
    colors <- outputColumn(df) |> nlevels() |> rainbow()
    if (hasAlpha) {
        alphaCoeff <- min(1, 10000 / nrow(df))
        scales::alpha(colors, alphaCoeff)
    } else {
        colors
    }
}

plotClasses <- function(cover) {
    proportions <- table(cover$type) |> prop.table()
    typeNames <- c(
        "Spruce/Fir",
        "Lodgepole Pine",
        "Ponderosa Pine",
        "Cottonwood/Willow",
        "Aspen",
        "Douglas-fir",
        "Krummholza"
    )
    par(mar = c(6, 4, 4, 4))
    colors <- paletteColors(cover)
    barplot(
        proportions,
        main = "Proportions des types d'arbres",
        names.arg = typeNames,
        col = colors,
        las = 2,
        cex.names = 0.7
    )
    legend(
        x = "topright",
        legend = typeNames,
        col = colors,
        pch = 16,
        horiz = FALSE 
    )
}

plotClasses(cover)
```

Boîtes à moustaches des variables explicatives numériques :

```{r}
# Boxplots of continuous variables depending on output.
plotBoxplots <- function(df) {
    for (varName in getNumericVariables(df)) {
        title <- paste0(
            "Boîte à moustaches de la variable\n",
            labelFromName(df, varName),
            " selon la variable ",
            outputLabel(df)
        )
        boxplot(
            df[[varName]] ~ outputColumn(df),
            col = paletteColors(df),
            main = title,
            xlab = outputLabel(df) |> capitalize(),
            ylab = labelFromName(df, varName) |> capitalize(),
            cex.main = 0.9
        )
    }
}

plotBoxplots(cover)
```

Pas mal de zéros pour les variables d'ombre : valeurs manquantes ?

Des outliers également : des histogrammes seraient les bienvenus.

```{r}
plotHistograms <- function(df) {
    for (varName in getNumericVariables(df)) {
        title <- paste0(
            "Histogramme de la variable\n",
            labelFromName(df, varName)
        )
        hist(
            df[[varName]],
            freq = FALSE,
            main = title,
            xlab = labelFromName(df, varName) |> capitalize(),
            ylab = "Densité",
            col = "blue",
            cex.main = 0.9
        )
    }
}

plotHistograms(cover)
```

En détaillant les histogrammes par type :

```{r}
plotOverlaidHistograms <- function(df) {
    for (varName in getNumericVariables(df)) {
        title <- paste0(
            "Histogramme de la variable ",
            labelFromName(df, varName)
        )
        p <- ggplot2::ggplot(cover, ggplot2::aes_string(
            x = varName, fill = "type", y = "..density..")) +
            ggplot2::geom_histogram(
                position = "identity", alpha = 0.4, bins = 50) +
            ggplot2::geom_density(alpha = 0.1, ggplot2::aes(colour = type)) +
            ggplot2::ggtitle(title)
        print(p)
    }
}

plotOverlaidHistograms(cover)
```

Fréquences pour les variables explicatives catégorielles :

```{r}
# Stacked plots of factors depending on output.
plotStacked <- function(df) {
    colors <- paletteColors(df)
    for (varName in getFactorVariables(df)) {
        title <- paste0(
            "Diagramme en bâtons du tableau de contingence \n",
            "de la variable ",
            labelFromName(df, varName),
            " selon la variable ",
            outputLabel(df)
        )
        outputColumn(df) |>
            (\(output) table(output, df[[varName]]))() |> 
            prop.table(margin = 2) |>
            barplot(
                col = colors, 
                main = title, 
                xlab = labelFromName(df, varName) |> capitalize(),
                ylab = outputLabel(df) |> capitalize(),
                cex.main = 0.9
                )
        legend(
            x = "bottom",
            legend = outputColumn(df) |> levels(),
            col = colors,
            pch = 16
        )
    }
}

plotStacked(cover)

```
Nuages de points 

```{r}
# 2D scatter plot for a given pair of explanatory variables.
plot2DScatter <- function(df, pairNames) {
    title <- paste0(
        outputLabel(df) |> capitalize(),
        " selon ",
        labelFromName(df, pairNames$x),
        " et ",
        labelFromName(df, pairNames$y)
    )
    plot(
        df[c(pairNames$x, pairNames$y)],
        main = title,
        xlab = labelFromName(df, pairNames$x) |> capitalize(),
        ylab = labelFromName(df, pairNames$y) |> capitalize(),
        cex.main = 0.9,
        col = paletteColors(df, hasAlpha = TRUE)[outputColumn(df)],
        pch = 16
    )
    legend(
        x = "topleft", 
        legend = outputColumn(df) |> levels(),
        col = paletteColors(df),
        pch = 16
    )
}

# Returns all pairs of explanatory variable names.
getPairsVarNames <- function(df) {
    varNames <- nonDummyVariables
    pairsNames <- list()
    for (i in 1:(length(varNames) - 1)) {
        for (j in (i + 1):length(varNames)) {
            pairNames <- list(x = varNames[i], y = varNames[j])
            pairsNames[[length(pairsNames) + 1]] <- pairNames
        }
    }
    pairsNames
}

# 2D scatter plots for every pair of explanatory variables.
plot2DScatters <- function(df) {
    for (pairNames in getPairsVarNames(df)) plot2DScatter(df, pairNames)
}


plot2DScatters(cover)
```

Une expression de trigonométrie relie altitude, pente, azimuth de la pente et ombre : la [loi des cosinus](https://en.wikipedia.org/wiki/Spherical_law_of_cosines).

Corrélogramme des variables continues, de la région et du sol.

```{r}
# Returns the explanatory variables.
explanatoryVariables <- function(df) {
    df[, names(df) != attr(df, "output")]
}

# Correlogram.
plotCorrelogram <- function(df, varNames = NULL) {
    dg <- explanatoryVariables(df)
    if (!is.null(varNames)) {
        dg <- dg[, varNames]
    }
    cor(dg) |> {\(corr) corrplot::corrplot(
        corr,
        title = "Corrélogramme",
        diag = FALSE,
        order = "hclust", 
        tl.col = "black", 
        tl.srt = 45
        )}()
}

nonDummyVariables <- c(getNumericVariables(cover), "region", "soil")
plotCorrelogram(cover, varNames = nonDummyVariables)
```
Quelques hypothèses.

* L'altitude est corrélée négativement à la pente. Seuls les arbres à basse altitude supportent la pente ?
* Les arbres en pente sont davantage à l'ombre ?
* L'altitude et le type de sol sont corrélés. Attention : le type de sol n'a pas de raison * a priori * d'être ordonné.
* La distance à la route et la distance au feu sont corrélés : les incendies sont provoqués par l'activité humaine ? Créer une variable de la somme des distances aux deux ?
* L'azimuth de la pente est corrélé à l'ombre ; voir l'article @stage76 pour une étude plus précise.
* La distance à l'eau est corrélée positivement à l'altitude : moins de sources d'eau en hauteur ?

Une autre visualisation synthétique :

```{r}
plotGGpairs <- function(df, varNames) {
    dg <- explanatoryVariables(df)[, varNames]
    GGally::ggpairs(dg, ggplot2::aes(color = paletteColors(df)[outputColumn(df)])) |> print()
}

set.seed(42)
plotGGpairs(
    cover[sample(1:nrow(cover), 10000),], 
    varNames = c("altitude", "slope", "distanceWater", "shadow")
    )
```

Analyse par composantes principales :

```{r}
# Gathers plots of principal component analysis.
plotPCA <- function(df, varNames) {
    pca <- explanatoryVariables(df)[, varNames] |> 
        {\(vars) prcomp(vars, center = TRUE, scale = TRUE)}()
    plotScatterPCA(pca, df)
    plotVariancePCA(pca)
    plotVariablesPCA(pca)
    plotContribPCA(pca)
}

# Plots transformed data from principal component analysis.
plotScatterPCA <- function(pca, df) {
    transformed <- cbind(as.data.frame(pca$x), outputColumn(df))
    title <- "Analyse par composantes principales"
    plot(
        transformed$PC1,
        transformed$PC2,
        main = title,
        xlab = "Premier axe",
        ylab = "Deuxième axe",
        col = paletteColors(df, hasAlpha = TRUE)[outputColumn(df)],
        pch = 16
        )
    legend(
        x = "topleft",
        title = outputLabel(df) |> capitalize(),
        legend = outputColumn(df) |> levels(),
        cex = 1,
        col = paletteColors(df),
        pch = 16
    )
}

# Plots explained variance from principal component analysis.
plotVariancePCA <- function(pca) {
    factoextra::fviz_eig(pca) +
        ggplot2::labs(
            title = "Variance expliquée",
            x = "Composantes principales",
            y = "Pourcentage de la variance expliquée"
        ) +
        ggplot2::theme_minimal() |> print()
}

# Plots transfomed variables from principal component analysis.
plotVariablesPCA <- function(pca) {
    factoextra::fviz_pca_var(
        pca,
        col.var = "contrib",
        repel = TRUE
    ) |> print()
}

# Plots top five contributions for first and second axis.
plotContribPCA <- function(pca) {
    factoextra::fviz_contrib(pca, choice="var", axes = 1, top = 5) |> print()
    factoextra::fviz_contrib(pca, choice="var", axes = 2, top = 5) |> print()
}

plotPCA(cover, varNames = nonDummyVariables)
```
Ça vaudrait le coup de :

* supprimer les variables d'ombres détaillées ;
* supprimer les variables de distance à l'eau détaillées.

```{r}
varNames <- nonDummyVariables[c(1, 2, 3, 6, 10, 11, 12, 13, 14)]
print(varNames)
plotPCA(cover, varNames = varNames)
```
```{r}
fitLasso <- function(splitted, varNames) {
    set.seed(42)
    for (i in 1:length(varNames)) {
       paste0("indice ", i, " : ", varNames[i]) |> print()
    }
    indices <- sample(1:nrow(splitted$XTraining), 10000)
    model <- glmnet::glmnet(
        as.matrix(splitted$XTraining[indices, varNames]),
        splitted$YTraining[indices],
        alpha = 1,
        family = "multinomial",
        grouped = TRUE,
        type.measure = "class"
    )
    plot(model, label = TRUE)
    modelCV <- glmnet::cv.glmnet(
        as.matrix(splitted$XTraining[indices, varNames]),
        splitted$YTraining[indices],
        alpha = 1,
        family = "multinomial",
        grouped = TRUE,
        type.measure = "class"
    )
    lambdaBest <- modelCV$lambda.1se
    print(lambdaBest)
    modelSingle <- glmnet::glmnet(
        as.matrix(splitted$XTraining[indices, varNames]),
        splitted$YTraining[indices],
        alpha = 1,
        family = "multinomial",
        grouped = TRUE,
        type.measure = "class",
        lambda = 0.05
    )
    coef(modelSingle) |> print()
    modelSingle
}

splitted <- splitData(cover) |> toMatrix()
model <- fitLasso(splitted, varNames = nonDummyVariables)
```


## Entraînement XGBoost

```{r}
df <- cover[, c(nonDummyVariables, "type")]
attr(df, "output") <- "type"
splitted <- splitData(df) |> toMatrix()
params <- list(subsample = 0.01)
model <- fitXgboostSoftmax(splitted, nrounds = 100, params = params)
print(model)
predictClass(model, splitted)
plot(
    model$evaluation_log,
    main = "Evolution de l'erreur out-of-bag en fonction du nombre d'arbres",
    xlab = "Nombre d'arbres",
    ylab = "Erreur",
    type = "l",
    col = "blue"
    )
```



```{r}
xgb.importance(
    feature_names = explanatoryVariablesNames(df), model = model) |>
        xgb.plot.importance()
```

