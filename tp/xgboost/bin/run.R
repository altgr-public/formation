import::here(formation, prepare)
import::here("R/process.R", processBCWD, processCover, processIris)

# If "Erreur : Cannot assign name to different value in the given environment. Name already in use",
# then reset environment with command "rm(list=ls())"
main <- function() {
    print("Lancement du script du thème XGBoost.")
    prepare(c(
        "caret", "DiagrammeR", "DiagrammeRsvg", "rsvg", "magrittr", "xgboost"))
    processIris()
    processCovertype()
    processBCWD()
}
