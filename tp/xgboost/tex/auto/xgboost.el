(TeX-add-style-hook
 "xgboost"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "a4paper" "11pt" "french")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("babel" "main=french" "english") ("xcolor" "dvipsnames") ("enumitem" "shortlabels")))
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontspec"
    "lmodern"
    "xspace"
    "babel"
    "amsfonts"
    "amssymb"
    "amsmath"
    "amsthm"
    "mathabx"
    "dsfont"
    "url"
    "hyperref"
    "xcolor"
    "eurosym"
    "graphicx"
    "caption"
    "subcaption"
    "listings"
    "enumitem")
   (TeX-add-symbols
    "N"
    "Z"
    "Q"
    "R"
    "D"
    "B"
    "V"
    "E"
    "signed")
   (LaTeX-add-bibliographies
    "biblio.bib")
   (LaTeX-add-amsthm-newtheorems
    "thm"
    "prop"
    "cor"
    "lem"
    "defn"
    "exm"
    "rem"
    "exo"
    "nota"
    "conv")
   (LaTeX-add-xcolor-definecolors
    "aliceblue"))
 :latex)

