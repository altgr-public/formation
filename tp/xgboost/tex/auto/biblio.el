(TeX-add-style-hook
 "biblio"
 (lambda ()
   (LaTeX-add-bibitems
    "friedman01"
    "chen16"
    "kumar20"
    "stage76"))
 :bibtex)

