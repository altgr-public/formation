\documentclass[a4paper,11pt,french]{article}
\usepackage[utf8]{inputenc}
\usepackage{fontspec}
\usepackage{lmodern}
\usepackage{xspace}
\usepackage[main=french,english]{babel}
\usepackage{amsfonts,amssymb,amsmath,amsthm,mathabx,dsfont}
\usepackage{url,hyperref}
\usepackage[dvipsnames]{xcolor}
\usepackage{eurosym}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{listings}

\theoremstyle{definition}
\newtheorem{thm}{Théorème}[section]
\newtheorem{prop}[thm]{Proposition}
\newtheorem{cor}[thm]{Corollaire}
\newtheorem{lem}[thm]{Lemme}
\newtheorem{defn}[thm]{Définition}
\newtheorem{exm}[thm]{Exemple}
\newtheorem{rem}[thm]{Remarque}
\newtheorem{exo}[thm]{Exercice}
\newtheorem{nota}[thm]{Notation}
\newtheorem{conv}[thm]{Convention}

\newcommand{\N}{\mathbf{N}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\D}{\mathbf{D}}
% \renewcommand{\C}{\mathbf{C}}
\newcommand{\B}{\mathbf{B}}

\definecolor{aliceblue}{rgb}{0.94, 0.97, 1.0}

\lstset{language=R,                                     % set programming language
    basicstyle=\small\ttfamily,                         % basic font style
    stringstyle=\color{DarkGreen},
    otherkeywords={0,1,2,3,4,5,6,7,8,9},
    keywordstyle=\color{Blue},                          % keyword style
    commentstyle=\ttfamily \color{DarkGreen},           % comment style
    backgroundcolor=\color{aliceblue},                  % background colour
    numbers=left,                                       % display line numbers on the left side
    numberstyle=\ttfamily\color{Gray}\footnotesize,     % line numbers
  }

% \DeclareMathOperator\spt{spt}
\DeclareMathOperator\V{Var}
\DeclareMathOperator\E{E}

\def\signed #1{{\leavevmode\unskip\nobreak\hfil\penalty50\hskip2em
  \hbox{}\nobreak\hfil(#1)%
  \parfillskip=0pt \finalhyphendemerits=0 \endgraf}}

% Change tiret en bullet pour les listes.
\usepackage[shortlabels]{enumitem}
\setlist[itemize]{label=\textbullet}

\author{AltGR}
\title{Travaux pratiques sur XGBoost}
\date{Vendredi 17 juin 2022}

\begin{document}

\maketitle

\section{Librairie \texttt{xgboost}}

\subsection{Généralités}

XGBoost (\textit{eXtreme Gradient Boosting}) est l'interface sous R d'une
librairie (libre) écrite en C++ et décrite dans l'article~\cite{chen16}. C'est une adaptation
de l'algorithme \textit{gradient boosting}, en français \og renforcement du
gradient \fg, introduit par J.H. Friedman dans l'article~\cite{friedman01}.

L'algorithme XGBoost possède de nombreux paramètres. Nous renvoyons à la
documentation
générale~\url{https://xgboost.readthedocs.io/en/latest/parameter.html}, à celle
de l'interface
R~\url{https://cran.r-project.org/web/packages/xgboost/xgboost.pdf} ou au
guide~\url{https://www.kaggle.com/code/prashant111/a-guide-on-xgboost-hyperparameters-tuning/notebook}
pour une description détaillée. Voici les principaux paramètres :
\begin{itemize}
  \item \texttt{nrounds}, le nombre d’itérations de boosting (mollement en
  rapport avec le nombre d'arbres \texttt{ntree} de \texttt{randomForest}) ;
  \item \texttt{colsample\_bytree} : pourcentage de variables sélectionnées pour
  construire un arbre (équivalent du nombre de variables \texttt{mtry}) ;
  \item \texttt{max\_depth} : profondeur maximale de chaque arbre (relié à
  \texttt{maxnodes} et \texttt{nodesize}) ;
  \item \texttt{eta} : taux d'apprentissage, contrôle la convergence vers un minimum ;
  \item \texttt{gamma} : diminution minimale du coût pour scinder un noeud ;
  \item \texttt{min\_child\_weight} : poids minimum nécessaire pour scinder un
  noeud.
\end{itemize}

Ajuster les paramètres de l'algorithme XGBoost est la principale difficulté de son utilisation.

\subsection{Pré-requis}

On utilisera :
\begin{itemize}
  \item la librairie \texttt{caret} pour la validation croisée.
  \item la librairie \texttt{xgboost} pour l'algorithme XGBoost.
  \item les librairies
  \begin{itemize}
    \item \texttt{Ckmeans.1d.dp}
    \item \texttt{DiagrammeR},
    \item \texttt{DiagrammeRsvg},
    \item \texttt{ggplot2},
    \item \texttt{rsvg},
  \end{itemize}
  pour la visualisation (et leurs dépendances).
\end{itemize}

Sous Linux, les paquets \texttt{librsvg2-dev} et \texttt{libnode-dev} sont
indispensables pour la visualisation.

\section{Analyse du jeu de données Iris}

On reprend le jeu de données Iris, déjà analysé dans le TP sur la forêt aléatoire.

\begin{enumerate}
  \item Définir une partition entre données d'entraînement et de validation.
  \item XGBoost a besoin que les classes soient des entiers commençant à 0 :
  convertir la colonne des espèces d'iris.
  \item Transformer la tableau de données en objet \texttt{xgb.DMatrix}.
  \item Ajuster l'algorithme XGBoost sur les données d'entraînement
  (\texttt{xgboost} avec la méthode \texttt{xgbTree} et l'objectif
  \texttt{multi:softmax}). Tester plusieurs paramètres !
  \item Classer les variables explicatives par ordre d'importance (\texttt{xgb.importance}).
  \item Tracer le premier arbre de décision (\texttt{xgb.plot.tree}).
  \item Prédire l'espèce sur les données de validation (\texttt{predict}) et
  donner la matrice de confusion complète (\texttt{confusionMatrix}).
\end{enumerate}

\section{Analyse du jeu de données Covertype}

Même démarche que dans la section précédente avec le jeu de données Covertype.

\section{Analyse du jeu de données Breast Cancer Wisconsin (Diagnosis)}

\subsection{Introduction}

Ce jeu de données regroupe 569 diagnostics de cancer du sein basés sur 30
variables explicatives obtenues en analysant des images de prélèvements.

Voir
\url{https://archive.ics.uci.edu/ml/datasets/breast+cancer+wisconsin+(diagnostic)}
pour plus de détails, ainsi que le fichier associé \texttt{wdbc.data} téléchargeable au lien \url{https://archive.ics.uci.edu/ml/machine-learning-databases/breast-cancer-wisconsin/wdbc.data}.

\subsection{Exploration}

Renommer le fichier \texttt{wdbc.data} et le déplacer dans
\texttt{data/public}.

Reprendre les mêmes tâches que pour les travaux pratiques sur la forêt aléatoire.

\subsection{XGBoost}

Même démarche que dans la section précédente avec le jeu de données Covertype.

\bibliographystyle{amsplain}
\bibliography{biblio.bib}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
