(TeX-add-style-hook
 "biblio"
 (lambda ()
   (LaTeX-add-bibitems
    "oshiro12"
    "probst17"
    "breiman01_forests"
    "probst19"
    "scornet17"
    "kumar20"))
 :bibtex)

