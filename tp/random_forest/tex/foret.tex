\documentclass[a4paper,11pt,french]{article}
\usepackage[utf8]{inputenc}
\usepackage{fontspec}
\usepackage{lmodern}
\usepackage{xspace}
\usepackage[main=french,english]{babel}
\usepackage{amsfonts,amssymb,amsmath,amsthm,mathabx,dsfont}
\usepackage{url,hyperref}
\usepackage[dvipsnames]{xcolor}
\usepackage{eurosym}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{listings}

\theoremstyle{definition}
\newtheorem{thm}{Théorème}[section]
\newtheorem{prop}[thm]{Proposition}
\newtheorem{cor}[thm]{Corollaire}
\newtheorem{lem}[thm]{Lemme}
\newtheorem{defn}[thm]{Définition}
\newtheorem{exm}[thm]{Exemple}
\newtheorem{rem}[thm]{Remarque}
\newtheorem{exo}[thm]{Exercice}
\newtheorem{nota}[thm]{Notation}
\newtheorem{conv}[thm]{Convention}

\newcommand{\N}{\mathbf{N}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\D}{\mathbf{D}}
% \renewcommand{\C}{\mathbf{C}}
\newcommand{\B}{\mathbf{B}}

\definecolor{aliceblue}{rgb}{0.94, 0.97, 1.0}
\definecolor{darkgreen}{rgb}{0, 0.39, 0}

\lstset{language=R,                                     % set programming language
    basicstyle=\small\ttfamily,                         % basic font style
    stringstyle=\color{darkgreen},
    otherkeywords={0,1,2,3,4,5,6,7,8,9},
    keywordstyle=\color{Blue},                          % keyword style
    commentstyle=\ttfamily \color{darkgreen},           % comment style
    backgroundcolor=\color{aliceblue},                  % background colour
    numbers=left,                                       % display line numbers on the left side
    numberstyle=\ttfamily\color{Gray}\footnotesize,     % line numbers
  }

% \DeclareMathOperator\spt{spt}
\DeclareMathOperator\V{Var}
\DeclareMathOperator\E{E}

\def\signed #1{{\leavevmode\unskip\nobreak\hfil\penalty50\hskip2em
  \hbox{}\nobreak\hfil(#1)%
  \parfillskip=0pt \finalhyphendemerits=0 \endgraf}}

% Change tiret en bullet pour les listes.
\usepackage[shortlabels]{enumitem}
\setlist[itemize]{label=\textbullet}

\author{AltGR}
\title{Travaux pratiques sur les forêts aléatoires}
\date{Vendredi 17 juin 2022}

\begin{document}

\maketitle

\section{Librairie \texttt{randomForest}}

\subsection{Généralités}

La librairie classique sous R pour les forêts aléatoires est
\texttt{randomForest}, hébergée ici :
\url{https://cran.r-project.org/web/packages/randomForest/index.html}. Cette
librairie est basée sur le code Fortran original de L. Breiman (l'auteur de
l'algorithme) et A. Cutler, disponible sur le site officiel :
\url{https://www.stat.berkeley.edu/~breiman/RandomForests/cc_software.htm}.

Il
y a d'ailleurs pas mal de choses intéressantes sur ce site, notamment la
mise en garde :
\begin{quote}
  RF is an example of a tool that is useful in doing analyses of scientific
  data.

  But the cleverest algorithms are no substitute for human intelligence and
  knowledge of the data in the problem.

  Take the output of random forests not as absolute truth, but as smart
  computer generated guesses that may be helpful in leading to a deeper
  understanding of the problem.
\end{quote}

La fonction d'entraînement principale de la librairie s'appelle également
\texttt{randomForest} et nous renvoyons à la documentation
\url{https://cran.r-project.org/web/packages/randomForest/randomForest.pdf}
pour la liste complète de ses paramètres. Nous discutons de l'effet de ces
paramètres dans les sous-sections suivantes.

D'autres implémentations sous R sont disponibles :
\begin{itemize}
  \item \texttt{ranger} ;
  \item \texttt{randomForestSRC} ;
  \item \texttt{Rborist} ;
  \item \texttt{obliqueRF} ;
  \item \texttt{randomUniformForest} ;
  \item \texttt{ParallelForest} ;
  \item \texttt{cforest} ;
  \item \texttt{SparkR}.
\end{itemize}

\subsection{Jeu de données artificiel}

Pour étudier l'influence de ces paramètres pour une classification, nous utilisons un jeu de données avec $X =
(X_i)_{i=1}^m$ et
\[
  Y = \mathds{1}\{X_1 + X_2 - X_3^2 + \epsilon \geqslant
  0\} \; ,
\]
où les $\{X_i,\,
i=1,\dotsc,m\}$ et $\epsilon$ sont des variables aléatoires i.i.d. de loi
normale centrée réduite. Il est simulé par la fonction suivante :

\begin{lstlisting}[language=R]
randomDataset <- function(nbrRows, nbrColumns) {
    stopifnot(nbrRows >= 1)
    stopifnot(nbrColumns >= 3)
    df <- iidGaussian(nbrRows, nbrColumns)
    colnames(df) <- paste0("X", 1:nbrColumns)
    df$Y <- df$X1 + df$X2 - df$X3 ^ 2 + rnorm(nbrRows) >= 0
    df
}
\end{lstlisting}

On peut raisonnablement choisir $\texttt{nbrRows} = 1000$ et
$\texttt{nbrColumns} = 10$. 

Nous renvoyons à l'article~\cite{probst19} pour une revue de la littérature et
une discussion générale sur l'ajustement des paramètres de la forêt aléatoire,
et l'article~\cite{scornet17} pour des résultats théoriques.

\subsection{Nombre d'arbres : \texttt{ntree}} 

Le risque empirique diminue en général avec
le nombre d'arbres \texttt{ntree}, mais :
\begin{itemize}
  \item le gain est décroissant et peu être marginal par rapport à
  l'augmentation du coût de calcul ;
  \item le risque empirique (et d'autres critères) n'est pas toujours une
  fonction décroissante du nombre d'arbres.
\end{itemize}

L'article original~\cite{breiman01_forests} recommande d'utiliser moins de
$1000$ arbres. L'article~\cite{oshiro12} observe qu'une centaine d'arbres est
convenable pour des jeux de données de quelques milliers
d'observations. L'article~\cite{probst17} confirme cette observation et donne
quelques résultats théoriques (notamment sur l'entropie croisée et le score de
Breier).

Dessiner un graphique montrant l'évolution de l'erreur \textit{out-of-bag} en
fonction du nombre d'arbres : méthode \texttt{plot} d'un modèle renvoyé par
\texttt{randomForest} et utilisation de la formule \texttt{Y \textasciitilde .} et du
paramètre \texttt{data}.

Séparer les données d'entraînement et de validation, puis dessiner un graphique
montrant l'évolution de l'erreur pour différentes valeurs de \texttt{ntree} :
utilisation des paramètres \texttt{x}, \texttt{y}, \texttt{xtest} et
\texttt{ytest}. Utiliser le champ $\texttt{test\$err.rate}$ du modèle renvoyé
par \texttt{randomForest}.

\subsection{Nombre de variables par arbres : \texttt{mtry}}

Augmenter le nombre de variables par arbres \texttt{mtry} :
\begin{itemize}
  \item diminue l'erreur
d'approximation de chaque arbre (si \texttt{mtry} = 1, chaque arbre se prononce
en fonction d'une seule variable, par exemple) ;
  \item augmente la corrélation entre les arbres, ce qui diminue la qualité de
  prédiction de la forêt.
\end{itemize}

Par défaut, \texttt{mtry} vaut $\sqrt{\texttt{nbrColumns}}$. Cette valeur est
empirique. L'article original~\cite{breiman01_forests} propose
$\lfloor\log_2(\texttt{nbrColumns} + 1)\rfloor$.

Dessiner un graphique montrant l'évolution de l'erreur quadratique moyenne
en fonction du nombre d'arbres, pour :
\begin{itemize}
  \item plusieurs valeurs de \texttt{nbrColumns} ;
  \item plusieurs valeurs de $\texttt{mtry}$, dont
$\sqrt{\texttt{nbrColumns}}$ et $\lfloor\log_2(\texttt{nbrColumns} +
1)\rfloor$.
\end{itemize}

Même exercice que précédemment avec un diagramme en bâtons portant sur l'erreur
de toute la forêt aléatoire totale en fonction de $\texttt{mtry}$. Essayer
également différentes valeurs pour \texttt{ntree}.

Rechercher automatiquement la meilleure valeur pour \texttt{mtry} en utilisant
la fonction \texttt{tuneRF}.

On pourrait également utiliser \texttt{expand.grid}, seule ou avec la fonction
\texttt{trainControl} du paquet \texttt{caret}. Le paquet \texttt{mlrHyperopt}
peut aussi aider.

\subsection{Cardinal des feuilles : \texttt{nodesize}}

Pour la classification, par défaut, chaque feuille de chaque arbre est associé
à une unique classe, donc $\texttt{nodesize} = 1$. Compromis biais/variance :
\begin{itemize}
  \item augmenter \texttt{nodesize} diminue donc la profondeur de l'arbre, et donc le risque de
  surapprentissage ;
  \item diminuer \texttt{nodesize} augmente l'erreur
d'approximation.
\end{itemize}

Même exercice que précédemment avec un diagramme en bâtons portant sur l'erreur
de toute la forêt aléatoire totale en fonction de $\texttt{nodesize}$. Essayer
également différentes valeurs pour \texttt{nbrColumns}.

Une analyse similaire peut être effectuée avec le paramètre \texttt{maxnodes},
ces deux paramètres influençant la profondeur des arbres. Utiliser la fonction
\texttt{treesize} pour tracer un histogramme du nombre de sommets par arbres.

\section{Analyse du jeu de données Iris}

\subsection{Introduction}

Le jeu de données Iris est peut-être le jeu de données le plus célèbre. Sa
première version a été présentée par R. Fischer dans un article en 1936. La
version étudiée comprend 150 observations de cinq variables relatives à des
iris :
\begin{itemize}
  \item longueur du sépale ;
  \item largeur du sépale ;
  \item longueur du pétale ;
  \item largeur du pétale ;
  \item espèce (Setosa, Versicolor, Virginica).
\end{itemize}

Voir \url{https://archive.ics.uci.edu/ml/datasets/iris} pour plus de détails.

\begin{figure}
  \centering
  \includegraphics[width=0.3\textwidth]{../img/petal_sepal.png}
  \caption{Les quatres variables explicatives.}
\end{figure}

\begin{figure}
  \centering
  \begin{subfigure}[b]{0.3\textwidth}
    \centering
    \includegraphics[width=\textwidth]{../img/setosa.jpg}
    \caption{Setosa}
  \end{subfigure}
  \begin{subfigure}[b]{0.3\textwidth}
    \centering
    \includegraphics[width=\textwidth]{../img/versicolor.jpg}
    \caption{Versicolor}
  \end{subfigure}
  \begin{subfigure}[b]{0.3\textwidth}
    \centering
    \includegraphics[width=\textwidth]{../img/virginica.jpg}
    \caption{Virginica}
  \end{subfigure}
  \caption{Les trois espèces d'iris, valeurs de la variable d'intérêt.}
\end{figure}

Les données sont contenues dans le fichier \url{/data/public/iris.csv}. Elles
sont également directement disponibles sous R via la commande :
\begin{lstlisting}[language=R]
  data(iris)
\end{lstlisting}

On cherche à prédire l'espèce en fonction des quatre caractéristiques
numériques. C'est donc une classification avec :
\begin{itemize}
  \item quatre variables explicative à valeurs dans $\R$, d'où $\mathcal{X}=\R^4$ ;
  \item une variable d'intérêt à valeurs discrètes dans $\mathcal{Y}=\{setosa, versicolor, virginica\}$.
\end{itemize}

\subsection{Exploration}

Représenter les données avec leurs classes.
\begin{enumerate}
  \item Vérifier que les données sont bien chargées.
  \item Tracer les boîtes à moustaches des quatre variables explicatives
  suivant l'espèce d'iris.
  \item Tracer six nuages de points en deux dimensions en choisissant toutes les
  combinaisons de deux variables explicatives distinctes.
  \item Appeler la fonction \texttt{ggpairs} du paquet \texttt{GGally} pour
  une visualisation synthétique des données.
  \item Tracer quatre nuages de points en trois dimensions en choisissant toutes les
  combinaisons de trois variables explicatives distinctes.
  \item Effectuer une analyse par composantes principales et tracer les deux
  premières composantes.
\end{enumerate}

\subsection{Forêt aléatoire}

On utilisera la librairie \texttt{randomForest}.

\begin{enumerate}
  \item Définir une partition entre données d'entraînement et de validation.
  \item Ajuster une forêt aléatoire sur les données d'entraînement avec des
  paramètres raisonnables  (\texttt{randomForest}).
  \item Classer les variables explicatives par ordre d'importance (\texttt{importance,varImpPlot}).
  \item Tracer l'effet marginal de la largeur de pétale sur l'appartenance à
  l'espèce Versicolor (\texttt{partialPlot}).
  \item Prédire l'espèce sur les données de validation (\texttt{predict}).
  \item Tracer le nuage de points en deux dimensions obtenu par positionnement
  multidimensionnel de la distance de proximité (\texttt{MDSplot}).
  \item Recommencer les tâches précédentes avec des paramètres non
  raisonnables afin de provoquer un surapprentissage.
\end{enumerate}

\section{Analyse du jeu de données Covertype}

\subsection{Introduction}

Le jeu de données Covertype décrit le type de couverture forestière pour 581
012 observations de 54 variables continues ou discrètes, notamment les suivantes :
\begin{itemize}
  \item altitude ;
  \item aspect ;
  \item pente ;
  \item distance à une source d'eau ;
  \item ensoleillement ;
  \item type de sol.
\end{itemize}

Voir la page \url{https://archive.ics.uci.edu/ml/datasets/ĉovertype} pour plus
de détails, ainsi que le fichier associé \texttt{covtype.info} à l'archive
téléchargeable au lien
\url{https://archive.ics.uci.edu/ml/machine-learning-databases/covtype/}.

L'article récent~\cite{kumar20} entraîne une forêt aléatoire sur ce jeu de
données (sans donner ses paramètres) et compare les performances d'autres
algorithmes. La page
web~\url{https://www.kaggle.com/code/osterburg/forest-cover-type/notebook}
  analyse ce jeu de données avec Python, pour une compétition
  Kaggle.

\subsection{Exploration}

Représenter les données avec leurs classes.
\begin{enumerate}
  \item Charger et vérifier les données.
  \item Effectuer une analyse par composantes principales et tracer les deux
  premières composantes.
\end{enumerate}

\subsection{Forêt aléatoire}

Voyons si les forêts aléatoires prédisent les forêts réelles.

On utilisera la librairie classique \texttt{randomForest} avec un
sous-échantillon des données en suivant les mêmes étapes qu'avec le jeu de
données Iris.

Puis, on se rabattra sur la librairie \texttt{ranger} pour traiter la totalité
des données. Le paquet \texttt{tuneRanger} peut aider à choisir automatiquement
les paramètres.

\begin{enumerate}
  \item Définir une partition entre données d'entraînement et de validation.
  \item Ajuster une forêt aléatoire sur les données d'entraînement avec des
  paramètres raisonnables  (\texttt{ranger}).
  \item Classer les variables explicatives par ordre d'importance (\texttt{importance}).
  \item Prédire l'espèce sur les données de validation (\texttt{predict}).
\end{enumerate}

\bibliographystyle{amsplain}
\bibliography{biblio.bib}

\end{document}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
