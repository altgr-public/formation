import::here(magrittr, "%>%")
import::here(ranger, importance, ranger)

import::here(
            formation,
            bindTrainingData,
            explanatoryVariablesNames,
            getPredictions,
            setDevice,
            splitData
        )

trainRandomForestCover <- function(cover) {
    splitted <- splitData(cover)
    model <- fitRandomForestCover(splitted, nbrTrees = 100)
    print(model)
    plotImportance(cover, model)
    getPredictions(model, splitted, isRanger = TRUE)
}

fitRandomForestCover <- function(splitted, nbrTrees) {
    trainingData <- bindTrainingData(splitted)
    ranger(
        type ~ .,
        data = trainingData,
        num.trees = nbrTrees,
        mtry = 15,
        importance = "impurity"
    )
}

plotImportance <- function(cover, model) {
    setDeviceCover("importance")
    sortResult <- ranger::importance(model) %>%
        sort(decreasing = TRUE, index.return = TRUE)
    variables <- attr(cover, "variables")
    rownames(variables) <- variables$name
    varNames <- explanatoryVariablesNames(cover)[sortResult$ix]
    varLabels <- variables[varNames, "label"]
    title <- "Importance des variables explicatives"
    barplot(
        height = sortResult$x,
        main = title,
        names = varLabels,
        col = "blue",
        las = 3,
        cex.names = 0.4
    )
    dev.off()
}

# Todo: réécrire setDevice. 
setDeviceCover <- function(graphicName) {
    paste0("../graphics/covertype/", graphicName,".pdf") %>% pdf
}
