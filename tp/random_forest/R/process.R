import::here(magrittr, "%>%")

import::here(formation, loadCover, loadIris)

import::here("explore.R", exploreIris, exploreCover)
import::here("cover.R", trainRandomForestCover)
import::here("iris.R", trainRandomForestIris)
import::here("noise.R", trainRandomForestNoise)

# Processes noisy dataset: studies the parameters of random forest.
processNoise <- function() {
    trainRandomForestNoise()
}

# Processes Iris dataset:
# - loads dataset;
# - plots some graphics;
# - trains a random forest.
processIris <- function() {
    iris <- loadIris(show = TRUE)
    exploreIris(iris)
    trainRandomForestIris(iris)
}

# Processes Covertype dataset:
# - loads dataset;
# - plots some graphics;
# - trains a random forest.
processCovertype <- function() {
    cover <- loadCover(show = TRUE)
    exploreCover(cover)
    trainRandomForestCover(cover)
}
