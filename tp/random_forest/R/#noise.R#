import::here(magrittr, "%>%")
import::here(randomForest, randomForest, treesize, tuneRF)

import::here(formation, bindTrainingData, getPredictions, setDevice, splitData)
import::here(generate.R, randomDataset)

# Processes noisy dataset: studies the parameters of random forest.
trainRandomForestNoise <- function() {
    plotErrorOobByNbrTrees()
    plotErrorByNbrTrees()
    for (nbrColumns in c(10, 50, 500)) {
        plotErrorByMtry(nbrColumns = nbrColumns)
        ## findBestMtry(nbrColumns = nbrColumns)
        plotErrorByNodesize(nbrColumns = nbrColumns)
    }
    for (nodesize in c(1, 10, 100)) {
        plotTreeSize(nodesize = nodesize)
    }
}

plotErrorOobByNbrTrees <- function(nbrTrees = 1000) {
    df <- randomDataset()
    set.seed(42)
   model <- randomForest(Y ~ ., data = df, ntree = nbrTrees, doTrace = TRUE)
    setDevice(df, "nbrTrees_oob")
    title <- "Évolution de l'erreur out-of-bag selon le nombre d'arbres"
    plot(model, main = title)
    legend(
        "topright",
        legend = c("Faux négatifs", "Taux d'erreur", "Faux positifs"),
        col = c("green", "black", "red"),
        lty = c(3, 1, 2),
        bg = "white"
        )
    dev.off()
}

plotErrorByNbrTrees <- function(nbrTrees = 1000) {
    df <- randomDataset()
    splitted <- splitData(df)
    set.seed(42)
    model <- randomForest(
        x = as.matrix(splitted$XTraining),
        y = splitted$YTraining,
        xtest = as.matrix(splitted$XTesting),
        ytest = splitted$YTesting,
        ntree = nbrTrees,
        doTrace = TRUE
    )
    setDevice(df, "nbrTrees_test")
    title <- "Évolution de l'erreur test selon le nombre d'arbres"
    plot(
        1:nbrTrees,
        model$test$err.rate[, 1],
        ylim = c(0, 0.5),
        main = title,
        xlab = "Nombre d'arbres",
        ylab = "Erreur",
        col = "black", 
        type = "l",
        lty = 1
    )
    lines(1:nbrTrees, model$test$err.rate[, 3], col = "green", type = "l", lty = 2)
    lines(1:nbrTrees, model$test$err.rate[, 2], col = "red", type = "l", lty = 3)
    legend(
        "topright",
        legend = c("Taux d'erreur", "Faux négatifs", "Faux positifs"),
        col = c("black", "green", "red"),
        lty = c(1, 2, 3),
        bg = "white"
        )
    dev.off()
}

plotErrorByMtry <- function(nbrColumns, nbrTrees = 1000) {
    df <- randomDataset(nbrColumns = nbrColumns)
    splitted <- splitData(df)
    n0 <- sqrt(nbrColumns)
    n1 <- log2(nbrColumns + 1)
    mtriesColumns <- nbrColumns / c(1, 2, 3, 5, 10, 100)
    mtries <- c(1:5, n0, n1, mtriesColumns) %>%
        floor %>%
        unique %>%
        sort %>%
        .[(. < nbrColumns + 1) & (. > 0)]
    errors <- rep(0, length(mtries))
    mtryColors <- length(mtries) %>% rainbow
    paste0("mtry_evolution_", nbrColumns, "_nbrTrees_", nbrTrees) %>%
        setDevice(df, .)
    title <- paste0(
        "Évolution de l'erreur selon le nombre d'arbres ",
        "\net le nombre de variables choisies (mtry) pour ",
        nbrColumns,
        " variables."
    )
    plot(
        1,
        type = "n",
        xlim = c(0, nbrTrees),
        ylim = c(0, 0.5),
        main = title,
        xlab = "Nombre d'arbres",
        ylab = "Erreur",
        cex.main = 0.9
    )
    for (i in 1:length(mtries)) {
        set.seed(42)
        error <- randomForest(
            x = as.matrix(splitted$XTraining),
            y = splitted$YTraining,
            xtest = as.matrix(splitted$XTesting),
            ytest = splitted$YTesting,
            ntree = nbrTrees,
            mtry = mtries[i]
        )
        lines(1:nbrTrees, error$test$err.rate[, 1], col = mtryColors[i])
        errors[i] <- error$test$err.rate[nrow(error$test$err.rate), 1]
        print("mtry : ")
        print(mtries[i])
        print("error : ")
        print(errors[i])
    }
    legend(
        "topright",
        title = "mtry",
        legend = mtries,
        col = mtryColors,
        pch = 15,
        bg = "white"
        )
    dev.off()
    paste0("mtry_final_", nbrColumns, "_nbrTrees_", nbrTrees) %>%
        setDevice(df, .)
    title <- paste0(
        "Erreur selon le nombre de variables choisies (mtry) \nparmi ",
        nbrColumns
    )
    barplot(
        height = errors,
        main = title,
        xlab = "Nombre de variables",
        ylab = "Erreur",
        ylim = c(0, 0.5),
        names = mtries,
        col = mtryColors,
        las = 3
    )
    dev.off()
}

findBestMtry <- function(nbrColumns, nbrTrees = 100) {
    set.seed(42)
    splitted <- randomDataset(nbrColumns) %>% splitData
    tuneRF(
        x = as.matrix(splitted$XTraining),
        y = splitted$YTraining,
        ntreeTry = nbrTrees,
        doTrace = TRUE,
        plot = TRUE
    )
}

plotErrorByNodesize <- function(nbrColumns, nbrTrees = 1000) {
    df <- randomDataset(nbrColumns = nbrColumns)
    splitted <- splitData(df)
    nodesizes <- c(1:10, 20, 50)
    errors <- rep(0, length(nodesizes))
    nodesizeColors <- length(nodesizes) %>% rainbow
    for (i in 1:length(nodesizes)) {
        set.seed(42)
        error <- randomForest(
            x = as.matrix(splitted$XTraining),
            y = splitted$YTraining,
            xtest = as.matrix(splitted$XTesting),
            ytest = splitted$YTesting,
            ntree = nbrTrees,
            nodesize = nodesizes[i]
        )
        errors[i] <- error$test$err.rate[nrow(error$test$err.rate), 1]
    }
    paste0("nodesize_", nbrColumns) %>% setDevice(df, .)
    title <- paste0(
        "Erreur selon le cardinal minimal des feuilles pour ",
        nbrColumns,
        " variables")
    barplot(
        height = errors,
        main = title,
        xlab = "Nombre de variables",
        ylab = "Erreur",
        names = nodesizes,
        col = nodesizeColors,
        las = 3
    )
    dev.off()
}

plotTreeSize <- function(nbrTrees = 1000, nodesize = 1) {
    df <- randomDataset(nbrColumns = 10)
    set.seed(42)
    model <- randomForest(
        Y ~ .,
        data = df,
        ntree = nbrTrees,
        nodesize = nodesize
    )
    paste0("treeSize_nodesize_", nodesize) %>% setDevice(df, .)
    title <- paste0(
        "Ordre des arbres pour des feuilles de cardinal minimum ",
        nodesize) 
    hist(
        treesize(model),
        main = title,
        col = "blue"
    )
    dev.off()
}
