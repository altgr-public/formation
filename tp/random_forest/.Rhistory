title <- "Évolution de l'erreur out-of-bag selon le nombre d'arbres"
plot(modelDefault, main = title, col = c("black", "green", "red", "blue"))
legend(
"topright",
legend = c("Taux d'erreur", "Setosa", "Versicolor", "Virginica"),
col = c("black", "green", "red", "blue"),
lty = c(1, 2, 3, 4),
bg = "white"
)
nbrTrees <- 1000
model <- fitRandomForestIris(splitted, method = "test", nbrTrees = nbrTrees)
print(model)
title <- "Évolution de l'erreur de validation selon le nombre d'arbres"
plot(
1:nbrTrees,
model$test$err.rate[, 1],
ylim = c(0, 0.5),
main = title,
xlab = "Nombre d'arbres",
ylab = "Erreur",
col = "black",
type = "l",
lty = 1
)
lines(1:nbrTrees, model$test$err.rate[, 2], col = "green", type = "l", lty = 2)
lines(1:nbrTrees, model$test$err.rate[, 3], col = "red", type = "l", lty = 3)
lines(1:nbrTrees, model$test$err.rate[, 4], col = "blue", type = "l", lty = 4)
legend(
"topright",
legend = c("Taux d'erreur", "Setosa", "Versicolor", "Virginica"),
col = c("black", "green", "red", "blue"),
lty = c(1, 2, 3, 4),
bg = "white"
)
nbrTrees <- 1000
mtries <- 1:4
errors <- rep(0, 4)
mtryColors <- rainbow(4)
title <- paste0(
"Évolution de l'erreur selon le nombre d'arbres ",
"\net le nombre de variables choisies (mtry)."
)
plot(
1,
type = "n",
xlim = c(0, nbrTrees),
ylim = c(0, 0.1),
main = title,
xlab = "Nombre d'arbres",
ylab = "Erreur",
cex.main = 0.9
)
for (i in 1:length(mtries)) {
error <- fitRandomForestIris(
splitted, method = "test", nbrTrees = nbrTrees, mtry = mtries[i])
lines(1:nbrTrees, error$test$err.rate[, 1], col = mtryColors[i])
errors[i] <- error$test$err.rate[nrow(error$test$err.rate), 1]
}
legend(
"topright",
title = "mtry",
legend = mtries,
col = mtryColors,
pch = 15,
bg = "white"
)
barplot(
height = errors,
main = "Erreur selon le nombre de variables choisies (mtry)",
xlab = "Nombre de variables",
ylab = "Erreur",
ylim = c(0, 0.1),
names = mtries,
col = mtryColors,
las = 3
)
tuneRF(x = as.matrix(splitted$XTraining), y = splitted$YTraining)
nbrTrees <- 1000
nodesizes <- c(1, 2, 3, 5, 10, 20, 30, 50, 75)
errors <- rep(0, length(nodesizes))
nodesizeColors <- length(nodesizes) |> rainbow()
for (i in 1:length(nodesizes)) {
error <- fitRandomForestIris(
splitted, method = "test", nbrTrees = nbrTrees, nodesize = nodesizes[i])
errors[i] <- error$test$err.rate[nrow(error$test$err.rate), 1]
}
barplot(
height = errors,
main = "Erreur selon le cardinal minimal des feuilles",
xlab = "Cardinal minimal",
ylab = "Erreur",
ylim = c(0, 0.5),
names = nodesizes,
col = nodesizeColors,
las = 3
)
nbrTrees <- 1000
# Attention : choisir maxnodes > 1.
listMaxnodes <- c(3, 5, 10, 20, 50, NULL)
errors <- rep(0, length(listMaxnodes))
maxnodesColors <- length(listMaxnodes) |> rainbow()
for (i in 1:length(listMaxnodes)) {
error <- fitRandomForestIris(
splitted, method = "test", nbrTrees = nbrTrees, maxnodes = listMaxnodes[i])
errors[i] <- error$test$err.rate[nrow(error$test$err.rate), 1]
}
barplot(
height = errors,
main = "Erreur selon le nombre maximum de feuilles",
xlab = "Nombre de feuilles",
ylab = "Erreur",
ylim = c(0, 0.1),
names = listMaxnodes,
col = maxnodesColors,
las = 3
)
plotTreeSize <- function(splitted, nodeSize = 1, maxNodes = NULL) {
nbrTrees <- 1000
model <- fitRandomForestIris(
splitted,
method = "formula",
nbrTrees = nbrTrees,
nodesize = nodeSize,
maxnodes = maxNodes
)
title <- paste0(
"Ordre des arbres pour des feuilles de cardinal minimum ",
nodeSize
)
if (!is.null(maxNodes)) {
title <- paste0(
title,
"\navec maximum ",
maxNodes,
" feuilles."
)
}
hist(treesize(model), main = title, col = "blue")
}
plotTreeSize(splitted, nodeSize = 1, maxNodes = NULL)
plotTreeSize(splitted, nodeSize = 30, maxNodes = NULL)
plotTreeSize(splitted, nodeSize = 1, maxNodes = 3)
plotTreeSize(splitted, nodeSize = 5, maxNodes = 20)
import::here(caret, train, trainControl)
?models
set.seed(42)
trainingData <- bindTrainingData(splitted)
nbrFolds <- 10
folds <- getFolds(trainingData, nbrFolds = 5)
listParams <- expand.grid(
mtry = 1:4,
nodesize = c(1, 2, 3, 5, 20, 50),
maxnodes = c(3, 5, 10, 50, NULL)
) |> {\(df) split(df, seq(nrow(df)))}()
bestError <- Inf
bestParams <- list()
for (params in listParams) {
params <- as.list(params)
sumErrors <- 0
for (fold in folds) {
model <- fitRandomForestIris(
fold,
method = "test",
nbrTrees = 1000,
mtry = params$mtry,
nodesize = params$nodesize,
maxnodes = params$maxnodes
)
error <- model$test$err.rate[nrow(model$test$err.rate), 1] |>
as.double()
sumErrors = sumErrors + error
}
paramsError <- sumErrors / nbrFolds
if (paramsError < bestError) {
print("Mise à jour : ")
print(paramsError)
print(params$mtry)
print(params$nodesize)
print(params$maxnodes)
bestError <- paramsError
bestParams <- params
}
}
list(
error = bestError,
mtry = bestParams$mtry,
nodesize = bestParams$nodesize,
maxnodes = bestParams$maxnodes
) |> print()
modelTuned <- fitRandomForestIris(
splitted, method = "test", mtry = 1, nodesize = 20, maxnodes = 3)
print(modelTuned)
modelDefault <- fitRandomForestIris(splitted, method = "test")
print(modelDefault)
varImpPlot(modelDefault, main = "Importance des variables explicatives défaut")
varImpPlot(modelTuned, main = "Importance des variables explicatives optimisé")
titles <- paste0(
"Espèce Versicolor selon la longueur de pétale ", c("défaut", "optimisé"))
testData <- cbind(splitted$XTest, splitted$YTest)
partialPlot(modelDefault, testData, Petal.Width, "versicolor", main = titles[1])
partialPlot(modelTuned, testData, Petal.Width, "versicolor", main = titles[2])
titles <- paste0("Positionnement multidimensionnel ", c("défaut", "optimisé"))
MDSplot(modelDefault, splitted$YTraining, main = titles[1])
MDSplot(modelTuned, splitted$YTraining, main = titles[2])
R.version
rstudioapi::versionInfo()
install.packages(c(
"devtools",
"import",
"e1071",
"factoextra",
"GGally",
"ggplot2",
"glmnet",
"scales",
"scatterplot3d",
"caret",
"rgl"
))
import::here(devtools, install_local)
install.packages("randomForest")
install.packages("ranger")
install.packages(c("devtools", "import", "e1071", "factoextra", "GGally", "ggplot2", "glmnet", "scales", "scatterplot3d", "caret", "rgl"))
install.packages("randomForest")
dirFormation <- "../../formation"
devtools::document(dirFormation)
install.packages("randomForest")
devtools::install_local(dirFormation, force = TRUE)
library(formation)
iris <- loadIris()
str(iris)
head(iris)
summary(iris)
plotBoxplots(iris)
plot2DScatters(iris)
plot3DScatters(iris)
import::here(rgl, plot3d)
options(rgl.printRglwidget = TRUE)
tripleNames <- list(x = "Petal.Length", y = "Petal.Width", z = "Sepal.Length")
plot3d(
iris[c(tripleNames$x, tripleNames$y, tripleNames$z)],
xlab = labelFromName(iris, tripleNames$x) |> capitalize(),
ylab = labelFromName(iris, tripleNames$y) |> capitalize(),
zlab = labelFromName(iris, tripleNames$z) |> capitalize(),
col = paletteColors(iris)[outputColumn(iris)],
pch = 16
)
plotPCA(iris)
plotCorrelogram(iris)
plotGGpairs(iris)
set.seed(42)
splitted <- splitData(iris)
import::here(
randomForest,
importance,
MDSplot,
partialPlot,
randomForest,
treesize,
tuneRF,
varImpPlot
)
fitRandomForestIris <- function(
splitted,
method = "formula",
nbrTrees = 1000,
mtry = 2,
maxnodes = NULL,
nodesize = 1
) {
stopifnot(method %in% c("formula", "test"))
set.seed(42)
if (method == "formula") {
trainingData <- bindTrainingData(splitted)
randomForest(
Species ~ .,
data = trainingData,
ntree = nbrTrees,
mtry = mtry,
nodesize = nodesize,
maxnodes = maxnodes,
replace = TRUE,
proximity = TRUE,
importance = TRUE,
do.trace = FALSE,
keep.forest = TRUE
)
} else {
randomForest(
x = as.matrix(splitted$XTraining),
y = splitted$YTraining,
xtest = as.matrix(splitted$XTesting),
ytest = splitted$YTesting,
ntree = nbrTrees,
mtry = mtry,
nodesize = nodesize,
maxnodes = maxnodes,
replace = TRUE,
proximity = TRUE,
importance = TRUE,
do.trace = FALSE,
keep.forest = TRUE
)
}
}
modelDefault <- fitRandomForestIris(splitted)
print(modelDefault)
getPredictions(modelDefault, splitted)
title <- "Évolution de l'erreur out-of-bag selon le nombre d'arbres"
plot(modelDefault, main = title, col = c("black", "green", "red", "blue"))
legend(
"topright",
legend = c("Taux d'erreur", "Setosa", "Versicolor", "Virginica"),
col = c("black", "green", "red", "blue"),
lty = c(1, 2, 3, 4),
bg = "white"
)
nbrTrees <- 1000
model <- fitRandomForestIris(splitted, method = "test", nbrTrees = nbrTrees)
print(model)
title <- "Évolution de l'erreur de validation selon le nombre d'arbres"
plot(
1:nbrTrees,
model$test$err.rate[, 1],
ylim = c(0, 0.5),
main = title,
xlab = "Nombre d'arbres",
ylab = "Erreur",
col = "black",
type = "l",
lty = 1
)
lines(1:nbrTrees, model$test$err.rate[, 2], col = "green", type = "l", lty = 2)
lines(1:nbrTrees, model$test$err.rate[, 3], col = "red", type = "l", lty = 3)
lines(1:nbrTrees, model$test$err.rate[, 4], col = "blue", type = "l", lty = 4)
legend(
"topright",
legend = c("Taux d'erreur", "Setosa", "Versicolor", "Virginica"),
col = c("black", "green", "red", "blue"),
lty = c(1, 2, 3, 4),
bg = "white"
)
nbrTrees <- 1000
mtries <- 1:4
errors <- rep(0, 4)
mtryColors <- rainbow(4)
title <- paste0(
"Évolution de l'erreur selon le nombre d'arbres ",
"\net le nombre de variables choisies (mtry)."
)
plot(
1,
type = "n",
xlim = c(0, nbrTrees),
ylim = c(0, 0.1),
main = title,
xlab = "Nombre d'arbres",
ylab = "Erreur",
cex.main = 0.9
)
for (i in 1:length(mtries)) {
error <- fitRandomForestIris(
splitted, method = "test", nbrTrees = nbrTrees, mtry = mtries[i])
lines(1:nbrTrees, error$test$err.rate[, 1], col = mtryColors[i])
errors[i] <- error$test$err.rate[nrow(error$test$err.rate), 1]
}
legend(
"topright",
title = "mtry",
legend = mtries,
col = mtryColors,
pch = 15,
bg = "white"
)
barplot(
height = errors,
main = "Erreur selon le nombre de variables choisies (mtry)",
xlab = "Nombre de variables",
ylab = "Erreur",
ylim = c(0, 0.1),
names = mtries,
col = mtryColors,
las = 3
)
tuneRF(x = as.matrix(splitted$XTraining), y = splitted$YTraining)
nbrTrees <- 1000
nodesizes <- c(1, 2, 3, 5, 10, 20, 30, 50, 75)
errors <- rep(0, length(nodesizes))
nodesizeColors <- length(nodesizes) |> rainbow()
for (i in 1:length(nodesizes)) {
error <- fitRandomForestIris(
splitted, method = "test", nbrTrees = nbrTrees, nodesize = nodesizes[i])
errors[i] <- error$test$err.rate[nrow(error$test$err.rate), 1]
}
barplot(
height = errors,
main = "Erreur selon le cardinal minimal des feuilles",
xlab = "Cardinal minimal",
ylab = "Erreur",
ylim = c(0, 0.5),
names = nodesizes,
col = nodesizeColors,
las = 3
)
nbrTrees <- 1000
# Attention : choisir maxnodes > 1.
listMaxnodes <- c(3, 5, 10, 20, 50, NULL)
errors <- rep(0, length(listMaxnodes))
maxnodesColors <- length(listMaxnodes) |> rainbow()
for (i in 1:length(listMaxnodes)) {
error <- fitRandomForestIris(
splitted, method = "test", nbrTrees = nbrTrees, maxnodes = listMaxnodes[i])
errors[i] <- error$test$err.rate[nrow(error$test$err.rate), 1]
}
barplot(
height = errors,
main = "Erreur selon le nombre maximum de feuilles",
xlab = "Nombre de feuilles",
ylab = "Erreur",
ylim = c(0, 0.1),
names = listMaxnodes,
col = maxnodesColors,
las = 3
)
plotTreeSize <- function(splitted, nodeSize = 1, maxNodes = NULL) {
nbrTrees <- 1000
model <- fitRandomForestIris(
splitted,
method = "formula",
nbrTrees = nbrTrees,
nodesize = nodeSize,
maxnodes = maxNodes
)
title <- paste0(
"Ordre des arbres pour des feuilles de cardinal minimum ",
nodeSize
)
if (!is.null(maxNodes)) {
title <- paste0(
title,
"\navec maximum ",
maxNodes,
" feuilles."
)
}
hist(treesize(model), main = title, col = "blue")
}
plotTreeSize(splitted, nodeSize = 1, maxNodes = NULL)
plotTreeSize(splitted, nodeSize = 30, maxNodes = NULL)
plotTreeSize(splitted, nodeSize = 1, maxNodes = 3)
plotTreeSize(splitted, nodeSize = 5, maxNodes = 20)
import::here(caret, train, trainControl)
?models
set.seed(42)
trainingData <- bindTrainingData(splitted)
nbrFolds <- 10
folds <- getFolds(trainingData, nbrFolds = 5)
listParams <- expand.grid(
mtry = 1:4,
nodesize = c(1, 2, 3, 5, 20, 50),
maxnodes = c(3, 5, 10, 50, NULL)
) |> {\(df) split(df, seq(nrow(df)))}()
bestError <- Inf
bestParams <- list()
for (params in listParams) {
params <- as.list(params)
sumErrors <- 0
for (fold in folds) {
model <- fitRandomForestIris(
fold,
method = "test",
nbrTrees = 1000,
mtry = params$mtry,
nodesize = params$nodesize,
maxnodes = params$maxnodes
)
error <- model$test$err.rate[nrow(model$test$err.rate), 1] |>
as.double()
sumErrors = sumErrors + error
}
paramsError <- sumErrors / nbrFolds
if (paramsError < bestError) {
print("Mise à jour : ")
print(paramsError)
print(params$mtry)
print(params$nodesize)
print(params$maxnodes)
bestError <- paramsError
bestParams <- params
}
}
list(
error = bestError,
mtry = bestParams$mtry,
nodesize = bestParams$nodesize,
maxnodes = bestParams$maxnodes
) |> print()
modelTuned <- fitRandomForestIris(
splitted, method = "test", mtry = 1, nodesize = 20, maxnodes = 3)
print(modelTuned)
modelDefault <- fitRandomForestIris(splitted, method = "test")
print(modelDefault)
varImpPlot(modelDefault, main = "Importance des variables explicatives défaut")
varImpPlot(modelTuned, main = "Importance des variables explicatives optimisé")
titles <- paste0(
"Espèce Versicolor selon la longueur de pétale ", c("défaut", "optimisé"))
testData <- cbind(splitted$XTest, splitted$YTest)
partialPlot(modelDefault, testData, Petal.Width, "versicolor", main = titles[1])
partialPlot(modelTuned, testData, Petal.Width, "versicolor", main = titles[2])
titles <- paste0("Positionnement multidimensionnel ", c("défaut", "optimisé"))
MDSplot(modelDefault, splitted$YTraining, main = titles[1])
MDSplot(modelTuned, splitted$YTraining, main = titles[2])
